# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :battler,
  ecto_repos: [Battler.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :battler, BattlerWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "cyBt0GPYZwwl7zoFVXTR+KcvK+dHx8Rejr9wl43AVTiuaxvByfsRnubu+wi8uYRy",
  render_errors: [view: BattlerWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Battler.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
