defmodule Battler.Repo do
  use Ecto.Repo,
    otp_app: :battler,
    adapter: Ecto.Adapters.Postgres
end
