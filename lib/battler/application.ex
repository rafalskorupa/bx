defmodule Battler.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      Battler.Repo,
      # Start the endpoint when the application starts
      BattlerWeb.Endpoint,
      {Battler.Counter, 0},
      absinthe_subscriptions(BattlerWeb.Endpoint)
      # Starts a worker by calling: Battler.Worker.start_link(arg)
      # {Battler.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Battler.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    BattlerWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp absinthe_subscriptions(name) do
    %{
      type: :supervisor,
      id: Absinthe.Subscription,
      start: {Absinthe.Subscription, :start_link, [name]}
    }
  end
end
