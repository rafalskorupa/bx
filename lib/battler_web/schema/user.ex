defmodule BattlerWeb.Schema.User do
  use Absinthe.Schema.Notation
  alias BatterWeb.Resolvers.Users, as: Resolver

  object :user do
    field :id, non_null(:string)
    field :name, non_null(:string)
    field :age, :integer
  end

  object :user_queries do
    @desc "Get list of all users"

    field :users, list_of(:user) do
      resolve(&Resolver.list_users/3)
    end

    field :user, :user do
      arg(:id, :id)
      resolve(&Resolver.get_user/3)
    end
  end

  object :user_mutations do
    @desc "Create new user"

    field :create_user, :user do
      arg(:name, non_null(:string))
      arg(:age, :integer)

      resolve(&Resolver.create_user/3)
    end
  end

  object :user_subscriptions do
    @desc "Last created user"

    field :last_created_user, :user do
      config(&Resolver.last_created_user_config/2)

      trigger(:create_user, topic: &Resolver.last_user_topic/1)

      resolve(&Resolver.user_from_mutation/3)
    end
  end
end
