defmodule BattlerWeb.Schema.Counter do
  use Absinthe.Schema.Notation
  alias BatterWeb.Resolvers.Counter, as: Resolver

  object :counter_queries do
    @desc "Get counter state"

    field :counter, :integer do
      resolve(&Resolver.value/3)
    end
  end

  object :counter_mutations do
    @desc "Create new user"

    field :increment_counter, :integer do
      resolve(&Resolver.increment/3)
    end

    field :decrement_counter, :integer do
      resolve(&Resolver.decrement/3)
    end
  end

  object :counter_subscriptions do
    @desc "Last created user"

    field :counter_state, :integer do
      config(&Resolver.counter_state_config/2)

      trigger(:increment_counter, topic: &Resolver.counter_state_topic/1)
      trigger(:decrement_counter, topic: &Resolver.counter_state_topic/1)

      resolve(&Resolver.counter_state_resolve/3)
    end
  end
end
