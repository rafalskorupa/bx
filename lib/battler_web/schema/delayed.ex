defmodule BattlerWeb.Schema.Delayed do
  use Absinthe.Schema.Notation
  alias BatterWeb.Resolvers.Delayed, as: Resolver

  object :delay_queries do
    field :resolve_later_one, :string do
      arg(:time, non_null(:integer))
      arg(:value, non_null(:string))

      resolve(&Resolver.delay_resolve/3)
    end

    field :resolve_later_two, :string do
      arg(:time, non_null(:integer))
      arg(:value, non_null(:string))

      resolve(&Resolver.delay_resolve/3)
    end
  end

  object :async_delay_queries do
    field :async_resolve_later_one, :string do
      arg(:time, non_null(:integer))
      arg(:value, non_null(:string))

      resolve(&Resolver.delay_async_resolve/3)
    end

    field :async_resolve_later_two, :string do
      arg(:time, non_null(:integer))
      arg(:value, non_null(:string))

      resolve(&Resolver.delay_async_resolve/3)
    end
  end
end
