defmodule BatterWeb.Resolvers.Delayed do
  use Absinthe.Schema.Notation

  def delay_resolve(_parent, %{time: time, value: value}, _resolution) do
    :timer.sleep(time)
    {:ok, value}
  end

  def delay_async_resolve(parent, %{time: time, value: value} = args, resolution) do
    async(fn ->
      delay_resolve(parent, args, resolution)
    end)
  end
end
