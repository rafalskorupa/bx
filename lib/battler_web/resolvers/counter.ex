defmodule BatterWeb.Resolvers.Counter do
  alias Battler.Counter

  def value(_, _, _), do: {:ok, Counter.value()}
  def increment(_, _, _), do: Counter.increment()
  def decrement(_, _, _), do: Counter.decrement()

  def counter_state_config(_, _), do: {:ok, topic: "Counter"}
  def counter_state_topic(_), do: "Counter"
  def counter_state_resolve(counter, _, _), do: {:ok, counter}
end
