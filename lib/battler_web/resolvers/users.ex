defmodule BatterWeb.Resolvers.Users do
  alias Battler.Users, as: Repository

  def get_user(_parent, %{id: id}, _resolution) do
    {:ok, Repository.get_user(id)}
  end

  def list_users(_parent, _args, _resolution) do
    {:ok, Repository.list_users()}
  end

  def create_user(_parent, args, _resolution) do
    Repository.create_user(args)
  end

  def last_created_user_config(_, _), do: {:ok, topic: "user"}
  def last_user_topic(_), do: "user"
  def user_from_mutation(user, _args, _resolution), do: {:ok, user}
end
