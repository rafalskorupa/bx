defmodule BattlerWeb.Schema do
  use Absinthe.Schema

  import_types(BattlerWeb.Schema.Counter)
  import_types(BattlerWeb.Schema.Delayed)
  import_types(BattlerWeb.Schema.User)

  query do
    import_fields(:counter_queries)
    import_fields(:user_queries)
    import_fields(:async_delay_queries)
    import_fields(:delay_queries)
  end

  mutation do
    import_fields(:user_mutations)
    import_fields(:counter_mutations)
  end

  subscription do
    import_fields(:user_subscriptions)
    import_fields(:counter_subscriptions)
  end
end
