defmodule BattlerWeb.Router do
  use BattlerWeb, :router

  forward "/graphq", Absinthe.Plug, schema: BattlerWeb.Schema

  forward "/graphiql", Absinthe.Plug.GraphiQL,
    schema: BattlerWeb.Schema,
    socket: BattlerWeb.UserSocket
end
